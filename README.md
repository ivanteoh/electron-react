# electron-react

It start with a minimal Electron application based on the [Quick Start Guide](http://electron.atom.io/docs/latest/tutorial/quick-start) within the Electron documentation.

Mainly this is electron react tutorial based on [Build a Music Player with React & Electron](https://scotch.io/tutorials/build-a-music-player-with-react-electron-i-setup-basic-concepts)

## To Use

From your command line:

```bash
# Install dependencies and run the app
npm install && npm start
```

Learn more about Electron and its API in the [documentation](http://electron.atom.io/docs/latest).

#### License [CC0 (Public Domain)](LICENSE.md)
